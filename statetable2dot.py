import csv

class Dotfile:
    
    def __init__(self):
        self.transitions = []
        self.actions = {}
        
    def addTransition(self, fromState, condition, toState):
        self.transitions.append({"fromState":fromState,"condition":condition,"toState":toState})
    
    def readTransitionsFromFile(self, fileName):
        with open(fileName, newline='') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';',skipinitialspace = True)
            for row in reader:
                self.addTransition(row["fromState"],row["condition"],row["toState"])

    def addStateAction(self, state, statement):
        if state in self.actions:
            self.actions[state].append(statement)
        else:
            self.actions[state]=[statement]
    
    def readStateActionsFromFile(self, fileName):
        with open(fileName, newline='') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';',skipinitialspace = True)
            for row in reader:
                self.addStateAction(row["state"],row["statement"])
                
    def writeDotFile(self,fileName):
        with open(fileName,'w') as dotFile:
            dotFile.write("digraph {\n")
            for state, actions in self.actions.items():
                actionString = '\\n'.join(actions)
                actionString=actionString.replace('"','\\"')
                dotFile.write(f"  {state} [shape=\"rect\" label=\"{state}\\n {actionString}\"];\n")
            dotFile.write("\n")  
            for t in self.transitions:
                t['condition']=t['condition'].replace('"','\\"')
                t['condition']=t['condition'].replace('||','||\\n')
                t['condition']=t['condition'].replace('&&','&&\\n')
                dotFile.write(f"  {t['fromState']} -> {t['toState']} [label = \"{t['condition']}\"];\n")
            dotFile.write("}") 
            
            
if __name__ == '__main__':
    d=Dotfile()
    d.readStateActionsFromFile("actions.csv")
    d.readTransitionsFromFile("statetable.csv")
    d.writeDotFile("statetable.dot")
