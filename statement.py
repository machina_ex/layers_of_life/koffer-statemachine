#!/usr/bin/env python3

# This example shows how to write a basic calculator with variables.
#

from lark import Lark, Transformer, v_args

class Statement:
    parser = Lark("""
        ?start: bin
              | VAR "=" bin -> assign_var

        ?bin: comp
            | bin "&&" comp      -> and_
            | bin "||" comp      -> or_
            | bin "^" comp       -> xor

        ?comp: sum
            | comp "==" sum     -> eq
            | comp "!=" sum     -> ne
            | comp "<" sum      -> lt
            | comp "<=" sum     -> le
            | comp ">" sum      -> gt
            | comp ">=" sum     -> ge

        ?sum: product
            | sum "+" product   -> add
            | sum "-" product   -> sub
            | sum "&" product   -> concat 

        ?product: atom
            | product "*" atom  -> mul
            | product "/" atom  -> div

        ?atom: NUMBER           -> number
             | ESCAPED_STRING_SINGLE -> string
             | ESCAPED_STRING   -> string
             | "-" atom         -> neg
             | "!" atom         -> not_
             | VAR              -> var
             | "(" bin ")"
             | "true"           -> true
             | "false"          -> false
             | "null"           -> null

        
        ESCAPED_STRING_SINGLE : "\'" STRING_ESC_INNER "\'"
        
        VAR: "{" STRING_INNER "}"

        STRING_ESC_INNER: STRING_INNER /(?<!\\\\)(\\\\\\\\)*?/
        STRING_INNER: /.*?/

        %import common.ESCAPED_STRING
        %import common.NUMBER
        %import common.WS_INLINE

        %ignore WS_INLINE
    """, parser='lalr')

    @v_args(inline=True)    # Affects the signatures of the methods
    class EvaluateStatements(Transformer):
        from operator import add, sub, mul, truediv as div, neg, or_, and_, eq, ne, lt, le, gt, ge, not_
        number = int
        null = lambda self, _: None
        true = lambda self, _: True
        false = lambda self, _: False

        def __init__(self):
            super().__init__()
            self.vars = {}
            self.assignments = {}

        def setVarDict(self, varDict):
            self.vars = varDict

        def setAssignments(self, assignments):
            self.assignments = assignments

        def string(self, s):
            return s[1:-1].replace('\\"', '"')
            
        def concat(self, a, b):
            return str(a)+str(b)

        def assign_var(self, name, value):
            self.vars[name[1:-1]] = value
            self.assignments[name[1:-1]] = value
            return value

        def var(self, name):
            try: 
                return int(self.vars[name[1:-1]])
            except ValueError:
                return self.vars[name[1:-1]]


    class FindVariables(Transformer):
        def __init__(self):
            self.variables = []

        def search(self, tree):
            self.variables = []
            self.currentTree = tree
            super().transform(tree)
            return self.variables

        def __getattr__(self, attr):
            return lambda y: None

        def VAR(self, name):
            self.variables.append(name[1:-1])
            return name

        def assign_var(self, name):
            self.variables.remove(name[0][1:-1])


    varFinder = FindVariables()
    condEvaluator = EvaluateStatements()

    def __init__(self, statement):
        self.statementStr = statement
        try:
            self.statement = Statement.parser.parse(statement)
        except Exception as error:
            print(self.statementStr)
            raise error
        self.variables = Statement.varFinder.search(self.statement)

    def evalCond(self, varDict):
        Statement.condEvaluator.setVarDict(varDict)
        return Statement.condEvaluator.transform(self.statement)

    def evalAssignment(self, varDict):
        Statement.condEvaluator.setVarDict(varDict)
        Statement.condEvaluator.setAssignments({})
        Statement.condEvaluator.transform(self.statement)
        return Statement.condEvaluator.assignments

class Transition(Statement):
    def __init__(self, fromState, condition, toState):
        self.fromState = fromState
        self.toState = toState
        super().__init__(condition)

class Assignment(Statement):
    def __init__(self, statement):
        super().__init__(statement)

if __name__ == '__main__':
    trans = Transition("hallo","{test} = '1'&&{test}", "hziuz")
    print(trans.variables)
    print(trans.evalCond({"test":"2"}))
