#/usr/bin/env python3

import csv
import json

zeiten_file = csv.DictReader(open("Zeitsichtungsgerät - ZEITEN.csv",encoding="utf8"))
actions = []
for line in zeiten_file:
    playlist = {}
    if line["STATE"]=="":
        continue
    if line['AUSSEN'] != "":
        playlist['AUSSEN'] = line['AUSSEN']
    if line['MITTE'] != "":
        playlist['MITTE'] = line['MITTE']

        
    if playlist != {}:
        playlist["STATE"] = line["STATE"]
        playlist["PLAYER"] = line["PLAYER"]
        actions.append((line["STATE"], ("{$KOFFER/setplay} = '"+json.dumps(playlist)+"'").replace("}'",",\"STARTTIME\":\"'&{$KOFFER/time/counter}&'\"}'")))
    
    lcd = {}
    
    if line["LCD_TEXT"] != "":
        if "\n" in line["LCD_TEXT"]:    
            lcd["LINE_1"] = line["LCD_TEXT"].split("\n")[0]
            lcd["LINE_2"] = line["LCD_TEXT"].split("\n")[1]
        elif len(line["LCD_TEXT"]) > 40:    
            lcd["LINE_1"] = line["LCD_TEXT"][:40]
            lcd["LINE_2"] = line["LCD_TEXT"][40:]
        else:
            lcd["LINE_1"] = line["LCD_TEXT"]
    if line["LCD_MODE"] != "":
        lcd["MODE"] = line["LCD_MODE"]

        
    if lcd != {}:
        actions.append((line["STATE"], "{$KOFFER/arduino} = '"+json.dumps(lcd)+"'"))
        
    if line["PRINT"] != "":
        actions.append((line["STATE"]+"_PRINT", "{$KOFFER/print} = '"+json.dumps({"TEXT":line["PRINT"],"BARCODE":line["BARCODE"]})+"'"))
        
    light_channels = {"L_TISCH":2, "L_BETT":4, "L_BAD":1, "L_DUNKEL":3,"L_KETTE":7,"L_RAMPE":8}

    for l in ["L_TISCH", "L_BETT", "L_BAD", "L_DUNKEL", "L_KETTE", "L_RAMPE"]:
        if line[l] != "":
            actions.append((line["STATE"], f"{{set/dmx/24:0A:C4:26:D6:A8/{light_channels[l]}}} = {int(line[l])}"))
        


with open('actionsZeiten.csv', 'w') as actionsfile:
    actionsfile.write("state;statement;\n")
    for action in actions:
        actionsfile.write(f"{action[0]};{action[1]};\n")
