#!/usr/bin/env python3
#
# parse condition find relevant topis for current state, subscribe..
# fromState;condition;toState
# currentState;{time/time} > {time/lastTime} +10;nextState
#

import csv

from statement import Transition, Assignment

class Statemachine:
    class Error(Exception):
        pass

    def __init__(self):
        self.transitionsByVariableName = {}
        self.transitionsByFromState = {}
        self.valuesByVariableName = {}
        self.stateVariables = {}
        self.lastStateVariables = {}
        self.stateActions = {}
        self.preReplaceVars = {}
        
        self.lastPrintTime = 0
        self.lastlastPrintTime = 0
        self.lastPrintString = ""
        
    def setStateVariable(self, stateVarName, value):
        self.stateVariables[stateVarName] = value

        if self.lastStateVariables != self.stateVariables:
            self.lastStateVariables = self.stateVariables.copy()
            self.setVariable(stateVarName, value)
            #try:
   
            #    if self.valuesByVariableName['time/counter'] != self.lastPrintTime:
            #        self.lastlastPrintTime=self.lastPrintTime
            #        self.lastPrintTime = self.valuesByVariableName['time/counter']
            #        print("")
            #    print(f"{int(self.valuesByVariableName['time/counter'])-int(self.lastlastPrintTime)} "
            #          f"{','.join(self.stateVariables.values())}  ",end="\r")   
            #except Exception as e:
            #    print(e)
            #    print (f"{','.join(self.stateVariables.values())}")
                
            self.doStateAction(value)
            
        
    def removeStateVariable(self, stateVarName):
        self.stateVariables.pop(stateVarName)

    def addTransition(self, fromState, condition, toState):
        for name, value in self.preReplaceVars.items():
            condition = condition.replace(name, value)
    
        t = Transition(fromState,condition,toState)
        
        if fromState in self.transitionsByFromState:
            self.transitionsByFromState[fromState].append(t)
        else:
            self.transitionsByFromState[fromState] = [t]
        
        for v in t.variables:
            if v in self.transitionsByVariableName:
                self.transitionsByVariableName[v].append(t)
            else:
                self.transitionsByVariableName[v] = [t]
    
    def setVariable(self, variableName, value):
        #if variableName == "time/counter":
        #    #print(f"{int(value)-int(self.lastPrintTime)}\r")
    
        if variableName in self.valuesByVariableName and \
            self.valuesByVariableName[variableName] == value:
            return False
        else:
            self.valuesByVariableName[variableName] = value
            return True
    
    def doStep(self):
        """ run the Statemachine and if necessary split into more variables/"processes" """

        #TODO: combine statevariables with the same Value
        removeStates = []
        setStates = {}
        for stateVarName, fromState in self.stateVariables.items():
            toStates = []
            
            if not fromState in self.transitionsByFromState:
                print(f"WARNING: reached dead end {fromState}, deleting the corresponding statevariable {stateVarName}")
                removeStates.append(stateVarName)
                continue
                
            for t in self.transitionsByFromState[fromState]:
                if not set(t.variables).issubset(self.valuesByVariableName):
                    print(f"WARNING: ignoring \"{t.fromState};{t.statementStr};{t.toState}\" because "
                          f"{set(t.variables).difference(self.valuesByVariableName)}"
                           " is not set.")
                    continue

                try:
                    if t.evalCond(self.valuesByVariableName):
                        toStates.append(t.toState)
                except Exception as e:
                    print(f"EXCEPTION: {e}")
                    for variable in t.variables:
                        print(f" {variable}={self.valuesByVariableName[variable]}")
                    print(f" {t.statementStr}")

            if len(toStates) == 1:
                print (f"{stateVarName}: {fromState} -> {toStates[0]}")
                setStates[stateVarName] = toStates[0]
            elif len(toStates) > 1:
                print (f"{stateVarName}: {fromState} ->")
                removeStates.append(stateVarName)
                for i, toState in enumerate(toStates,start=1):
                    print (f"  {stateVarName}/{toState} = {toState}")
                    setStates[f"{stateVarName}/{toState}"] = toState
        
        for s in removeStates:
            self.removeStateVariable(s)

        for k,v in setStates.items():
            self.setStateVariable(k,v)
        
        return (len(removeStates) > 0 or len(setStates) > 0) 

#    def runStepByChangedVariable(self, variableName):
#        for t in self.transitionsByVariableName[variableName]:
#            if (t.fromState == self.currentState):
#                if t.evalCond(self.valuesByVariableName):
#                    nextState = t.toState
   
    def getNotInitializedButUsedVars(self):
        notInitialized = []
        for k, v in self.transitionsByVariableName.items():
            if k not in self.valuesByVariableName:
                notInitialized.append(k)
        return notInitialized
        
   
    def readTransitionsFromFile(self, fileName):
        with open(fileName, newline='', encoding="utf8") as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';',skipinitialspace = True)
            for row in reader:
                self.addTransition(row["fromState"],row["condition"],row["toState"])

    def checkForDeadEnds(self):
        pass


    def addStateAction(self, state, action):
        for name,value in self.preReplaceVars.items():
            action = action.replace(name,value)
        if state in self.stateActions:
            self.stateActions[state].append(Assignment(action))
        else:
            self.stateActions[state] = [Assignment(action)]

    def doStateAction(self, state):
        if state in self.stateActions:
            for stateAction in self.stateActions[state]:
                if not set(stateAction.variables).issubset(self.valuesByVariableName):
                    print(f"WARNING: ignoring \"{state}: {stateAction.statementStr}\" because "
                          f"{set(stateAction.variables).difference(self.valuesByVariableName)}"
                           " is not set.")
                    continue
                name, value = stateAction.evalAssignment(self.valuesByVariableName).popitem()
                #print (f"{state}: {stateAction.statementStr} = {value}")
                self.setVariable(name, value)

    def readStateActionsFromFile(self, fileName):
        with open(fileName, newline='', encoding="utf8") as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';',skipinitialspace = True)
            for row in reader:
                self.addStateAction(row["state"],row["statement"])
                
                
    def addPreReplaceVar(self,name,value):
        self.preReplaceVars[name] = value
        
    def readPreReplaceVarsFromFile(self, fileName):
        with open(fileName, newline='', encoding="utf8") as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';',skipinitialspace = True)
            for row in reader:
                self.addPreReplaceVar(row["name"],row["value"])


if __name__ == '__main__':
    statemachine = Statemachine()
    statemachine.setStateVariable("state/initital","start")
    statemachine.addTransition("start","{test}","middle1")
    statemachine.addTransition("start","{test}","middle2")
    print(statemachine.getNotInitializedButUsedVars())
    statemachine.setVariable("test",0)
    print(statemachine.getNotInitializedButUsedVars())
    statemachine.doStep()
    statemachine.setVariable("test",1)
    statemachine.doStep()
    statemachine.addTransition("middle1","1","end")
    statemachine.addTransition("middle2","1","end")
    statemachine.doStep()
    statemachine.readTransitionsFromFile("statetable.csv")
    statemachine.setStateVariable("state/initital","start")
    statemachine.doStep()
    statemachine.readStateActionsFromFile("actions.csv")
    statemachine.setStateVariable("state/initital","start")
    statemachine.doStep()
