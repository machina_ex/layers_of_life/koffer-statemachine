#!/usr/bin/env python3

import socket

import paho.mqtt.client as mqtt

from statemachine import Statemachine



class MQTTStatemachine(Statemachine):

    class Error(Exception):
        pass

    class ConnectionError(Statemachine.Error):
        pass

    def __init__(self, host, port):
        self.mqtt_client = mqtt.Client()
        self.mqtt_client.on_message = self._on_message
        if self.mqtt_client.connect(host, port) != mqtt.MQTT_ERR_SUCCESS:
            raise MQTTStatemachine.ConnectionError("could not connect")

        super().__init__()

    def setStateVariable(self, stateVarName, value):
        self.mqtt_client.publish(stateVarName, value, 2, False)
        self.mqtt_client.subscribe((stateVarName,2))
        super().setStateVariable(stateVarName, value)

    def removeStateVariable(self, stateVarName):
        self.mqtt_client.unsubscribe(stateVarName)
        super().removeStateVariable(stateVarName)

    def setVariable(self, variableName, value):
        self.mqtt_client.publish(variableName, value, 2, False)
        self.mqtt_client.subscribe((variableName, 2))
        super().setVariable(variableName, value)

    def subscribeToVariables(self):
        l = [(topic, 2) for topic in self.transitionsByVariableName.keys()]
        l.append(("set/"+socket.gethostname()+"/state/#", 2))
        print(f"subscribing to: {l}")
        self.mqtt_client.subscribe(l)

    def loopForever(self):
        self.mqtt_client.loop_forever()

    def _on_message(self, client, userdata , msg):
        #print(msg.topic + "=" + str(msg.payload,"utf8"))
        try:
            #if msg.topic.startswith("vars/"):
            #    return
            
            if msg.topic.startswith("set/"+socket.gethostname()+"/state/"):
                topicStr = msg.topic.replace("set/","",1)
                super().setStateVariable(topicStr, str(msg.payload,"utf8"))
                super().setVariable(topicStr, str(msg.payload,"utf8"))  
            else:
                if super().setVariable(msg.topic, str(msg.payload,"utf8")):
                    while self.doStep(): # do all steps until there is nothing more to do
                        pass 
        except Exception as e:
            print(e)
            
            

    def _on_disconnect(self, client, userdata, rc, properties=None):
        raise MQTTStatemachine.ConnectionError("disconnect")

if __name__ == '__main__':
    statemachine = MQTTStatemachine("localhost", 1883)
    #statemachine = MQTTStatemachine("192.168.88.10", 1883)
    statemachine.setStateVariable(socket.gethostname()+"/state/START","START")
    statemachine.readPreReplaceVarsFromFile("constants.csv")
    statemachine.addPreReplaceVar("$KOFFER",socket.gethostname())
    statemachine.readStateActionsFromFile("actions.csv")
    statemachine.readStateActionsFromFile("actionsZeiten.csv")
    statemachine.readTransitionsFromFile("statetable.csv")
    statemachine.subscribeToVariables()
    statemachine.loopForever()

