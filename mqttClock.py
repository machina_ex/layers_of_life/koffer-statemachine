#!/usr/bin/env python3

import socket

import paho.mqtt.client as mqtt
import time

class MQTTClock:

    class Error(Exception):
        pass

    class ConnectionError(Exception):
        pass

    def __init__(self, host, port):
        self.mqtt_client = mqtt.Client()
        self.mqtt_client.on_message = self._on_message
        if self.mqtt_client.connect(host, port) != mqtt.MQTT_ERR_SUCCESS:
            raise self.ConnectionError("could not connect")


    def loopForever(self):
        timeCounter = 0
        while 1:
            self.mqtt_client.loop()
            self.mqtt_client.publish(socket.gethostname()+"/time/counter", timeCounter, 2, True)
            timeCounter = timeCounter + 1
            time.sleep(1)

    def _on_message(self, client, userdata , msg):
        print(msg.topic + " " + str(msg.payload))
        if msg.topic.startswith("state/"):
            super().setStateVariable(msg.topic, str(msg.payload,"utf8"))
        else:
            super().setVariable(msg.topic, str(msg.payload,"utf8"))
            self.doStep()

    def _on_disconnect(self, client, userdata, rc, properties=None):
        raise MQTTClock.ConnectionError("disconnect")

if __name__ == '__main__':
    statemachine = MQTTClock("localhost", 1883)
    #statemachine = MQTTClock("192.168.88.10", 1883)
    statemachine.loopForever()
